const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const should = chai.should();

chai.use(chaiHttp);

describe("RESTFul Course API", () => {
    // Test case cho API get all course
    describe("GET all courses", () => {
        it("It should get all the course of database", (done) => {
            chai.request(server)
                .get("/api/v1/courses")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.data.should.be.a('array');
                    
                    done();
                })
        })
    })
})
